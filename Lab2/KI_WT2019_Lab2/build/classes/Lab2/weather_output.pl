/*   
    This file manages the output
    of the weather expert system
*/

output:-
	print_facts(tomorrow(_)).

print_facts(X):-
    fact(X,Y),
    write(X),write('\t: '),
    format('~4f',Y),
    writeln(''),fail;true.

