/* 
    Main program reads the following prolog subprograms 
*/

:- 
    [%weather_input,
     weather_rules,
     weather_output].

% Main clause to activate the weather expert system 
forecast:-
    writeln('Weather forecast is'),
    process,  % processes the rules
    output.   % manages all output activities

