/*
  This file processes all rules
*/

%Operator definitions 
:- op(800, fx, if).
:- op(700, yfx, then).
:- op(200, yfx, and).
:- dynamic fact/2.

% Rule 1
if today(rain) then tomorrow(rain) : 0.5.

% Rule 2 
if today(dry) then tomorrow(dry) : 0.5.

% Rule 3
if today(rain) and rainfall(low) 
	then tomorrow(dry) : 0.6.

% Rule 4
if today(rain) and rainfall(low) and temperature(cold)
	then tomorrow(dry) : 0.7.

% Rule 5
if today(dry) and temperature(warm) 
	then tomorrow(rain) : 0.65.

% Rule 6
if today(dry) and temperature(warm) and sky(overcast) 
	then tomorrow(rain) : 0.55.



min(Num1, Num2, Ret) :-
	Num1 = Num2,
	Ret is Num2.

min(Num1, Num2, Ret) :- 
	Num1 < Num2,
	Ret is Num1.

min(Num1, Num2, Ret) :- 
	Num1 > Num2,
	Ret is Num2.

is_true(Premise,CF) :- 
	fact(Premise,CF).

composed_fact(Premise1,CF) :-
	is_true(Premise1,CF).
	
composed_fact(Premise1 and Premise2, CF) :-
	composed_fact(Premise1,CFa),
	composed_fact(Premise2,CFb),
	min(CFa, CFb, CF).


new_forecast(Forecast,NewCF) :-
	if X then Forecast : CFr,
	composed_fact(X,CFe),
	NewCF is CFe * CFr.

calculateTotalCF(CFa,CFb,CF_Result) :-
	CF_Result is CFa + CFb * (1 - CFa).


factIsThere(Conclusion,CF,CF_Result) :-	
	fact(Conclusion,CFa),
	calculateTotalCF(CFa,CF,CF_Result),
	retract(fact(Conclusion, _)).
	

factIsThere(Conclusion, CF, CF) :- 
	\+ fact(Conclusion, _).

pruefe(Regel, X) :- 
	\+ is_true(Regel,X),!,
	retract(fact(Regel, X)),
	calculateTotalCF(X,Xbb, Xb),
	assert(fact(Regel, Xb)).

getforecast(X,Y) :-
    forecast,
    fact(tomorrow(X), Y).


% Main clause to process all rules and all cf-values
process:-
	writeln('... processing rules ...'),
	new_forecast(Conclusion,CF),
	factIsThere(Conclusion, CF,CF_Result),
	assert(fact(Conclusion, CF_Result)),
	fail;true.

	
	
