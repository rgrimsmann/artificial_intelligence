package Lab2;

import java.util.Map;
import java.util.Scanner;
import org.jpl7.JPL;
import org.jpl7.Query;
import org.jpl7.Term;

public class Weather {

    public static void checkVersion() {
        Query.hasSolution("use_module(library(jpl))");
        Term swi = Query.oneSolution("current_prolog_flag(version_data,Swi)").get("Swi");
        System.out.println("swipl.version = " + swi.arg(1) + "." + swi.arg(2) + "." + swi.arg(3));
        System.out.println("swipl.home = " + Query.oneSolution("current_prolog_flag(home,Home)").get("Home").name());
        System.out.println("jpl.jar = " + JPL.version_string());
    }

    public static boolean hasElement(String arg[], String cmp) {

        for (String s : arg) {
            if (s.equals(cmp)) {
                return true;
            }

        }
        return false;

    }

    public static void main(String argv[]) {
        Map<String, Term>[] res;

        // Liefert Information über SWI-Prolog und JPL
        checkVersion();

        // Queries verwenden Strings, in denen Prologanfragen formuliert werden  
        // Passen Sie zum Einlesen der Prolog Wissenbasis den Pfad an, z.B. '$HOME/ai-lab/v2/weather_control.pl'
        String t1 = "consult('/home/rgn/NetBeansProjects/KI_WT2019_Lab2/src/Lab2/weather_control.pl')";
        // Ausgabe von Prolog ist in diesem Fall ein boole'scher Wert
        System.out.println(t1 + " " + (Query.hasSolution(t1) ? "succeeded" : "failed"));

        String weatherToday[] = {"rain", "dry"};
        String temperature[] = {"cold", "warm", "unknown"};
        String rainfall[] = {"low", "high", "unknown"};
        String sky[] = {"clear", "overcast", "unknown"};

        Boolean run = true;
        Scanner scanner = new Scanner(System.in);

        String weather = "rain";
        String temp = "cold";
        double t_ctr = 0.9;
        String rain = "low";
        double r_ctr = 0.8;
        String horizon = "unknown";
        double h_ctr = .5;
        double d;
        boolean is_set = false;//!weather.isEmpty();

        while (run == true) {
            System.out.println("Welcome to wether forecast system 5000! \n Press Enter to start!\n \n");
            String eingabe = scanner.nextLine();
            if (eingabe.equals("quit")) {
                run = false;
            }
            if (!is_set) {
                do {
                    System.out.println("What is the weather today?(dry,rain)");
                    eingabe = scanner.nextLine();
                    if (hasElement(weatherToday, eingabe)) {
                        weather = eingabe;
                    }
                } while (weather.equals(""));

                do {
                    System.out.println("What is the temperature today?(cold, warm, unknown)");
                    eingabe = scanner.nextLine();
                    if (hasElement(temperature, eingabe)) {
                        temp = eingabe;
                    }
                    do {
                        System.out.println("To what degree? [-1.0 - 1.0]");

                        String tmp = scanner.nextLine();
                        t_ctr = Double.parseDouble(tmp);
                    } while (!(t_ctr >= -1.0 && t_ctr <= 1.0));
                } while (temp.equals(""));
                do {
                    System.out.println("hat is the rainfall today?(low, high, unknown)");
                    eingabe = scanner.nextLine();
                    if (hasElement(rainfall, eingabe)) {
                        rain = eingabe;
                    }
                    do {
                        System.out.println("To what degree? [-1.0 - 1.0]");
                        String tmp = scanner.nextLine();
                        r_ctr = Double.parseDouble(tmp);
                    } while (!(r_ctr >= -1.0 && r_ctr <= 1.0));
                } while (rain.equals(""));

                do {
                    System.out.println("how is the sky today?(clear, overcast, unknown)");
                    eingabe = scanner.nextLine();
                    if (hasElement(sky, eingabe)) {
                        horizon = eingabe;
                    }
                    do {
                        System.out.println("To what degree? [-1.0 - 1.0]");
                        String tmp = scanner.nextLine();
                        h_ctr = Double.parseDouble(tmp);
                    } while (!(h_ctr >= -1.0 && h_ctr <= 1.0));
                } while (horizon.equals(""));
            }
            String prolog_question = ""; //Irgendwie muss man die fakten angeben assert(fact(today(rain), 1.0)) 
            if (!weather.contains("assert")) {
                weather = "assert(fact(today(" + weather + "), 1.0)).";
                if (!temp.equals("unknown")) {
                    temp = "assert(fact(temperature(" + temp + ")," + t_ctr + ")).";
                }
                if (!rain.equals("unknown")) {
                    rain = "assert(fact(rainfall(" + rain + ")," + r_ctr + ")).";
                }
                if (!horizon.equals("unknown")) {
                    horizon = "assert(fact(sky(" + horizon + ")," + h_ctr + ")).";
                }
            }
            //Dinge in Prolog ausführen
            //forecast(). ausführen
            //Ergebnis liefern 
            Query temp_Q = temp.equals("unknown") ? null : new Query(temp);
            Query weather_Q = weather.equals("unknown") ? null : new Query(weather);
            Query horizon_Q = horizon.equals("unknown") ? null : new Query(horizon);

            if ((temp_Q != null && temp_Q.hasSolution())) {
                System.out.println("Assert " + temp_Q.toString() + "to prolog!");
            }
            if ((weather_Q != null && weather_Q.hasSolution())) {
                System.out.println("Assert " + weather_Q.toString() + "to prolog!");
            }
            if ((horizon_Q != null && horizon_Q.hasSolution())) {
                System.out.println("Assert " + horizon_Q.toString() + "to prolog!");
            }

            //Ausgabe der forecast().
            Query forecast = new Query("getforecast(X,Y)");

            // Map[] solution_forecast = forecast.allSolutions();
            System.out.println("Query: " + forecast.toString());
            while (forecast.hasNext()) {
                Map<String, Term> map = forecast.next();
                System.out.println("Weather tomorrow: " + map.get("X") + " CF: " + map.get("Y"));
            }
           /*
            //TEST der Variablen
            Query testAssert = new Query("fact(X,Y).");
            Map[] solution = testAssert.allSolutions();
            System.out.println("Query: " + testAssert.toString());

            for (Map<String, Term> m : solution) {
                System.out.println("X = " + m.get("X"));
                System.out.println("Y = " + m.get("Y"));
            }*/
            run = false;
        }
    }

}



