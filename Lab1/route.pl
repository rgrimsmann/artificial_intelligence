stadt(arad).
stadt(sibiu).
stadt(timisoara).
stadt(lugoj).
stadt(mehadia).
stadt(craiova).
stadt(pitesti).
stadt(fagaras).
stadt(bukarest).

distanz(arad,sibiu,140).
distanz(arad,timisoara,118).
distanz(arad,fagaras,239).

distanz(timisoara,lugoj,111).
distanz(sibiu,pitesti,177).
distanz(fagaras,bukarest,211).
distanz(pitesti,bukarest,101).
distanz(pitesti,craiova,138).

distanz(lugoj,mehadia,70).
distanz(mehadia,craiova,195).





member(A,[A|_]).
member(A,[_|B]) :- member(A,B). 

route(Start,Ziel,Ergebnis) :-
	route2(Start,Ziel,[Start],Ergebnis).

route2(Start,Ziel,_,[Start,Ziel]):-
	reise(Start,Ziel).

route2(Start, Ziel, Variable, [Start| Ergebnis]) :-
	reise(Start, Z),
	\+ member(Z, Variable),
	route2(Z, Ziel, [Z|Variable ],Ergebnis),
	Z \= Ziel.

reise(Start,Ziel) :- distanz(Start,Ziel,_).
reise(Start,Ziel) :- distanz(Ziel,Start,_).





routeMitEntfernung(Start,Ziel,Ergebnis, Entfernung) :-
	routeMitEntfernung2(Start, Ziel,[Start], Ergebnis, Entfernung1),
	Entfernung is Entfernung1 + 0.
	
routeMitEntfernung2(Start,Ziel,_,[Start,Ziel],Entfernung):-
	reiseMitEntfernung(Start,Ziel,Distance),
	Entfernung is 0 + Distance.
	
routeMitEntfernung2(Start, Ziel, Variable, [Start| Ergebnis], Entfernung) :-
	reiseMitEntfernung(Start, Z, Distance),
	\+ member(Z, Variable),
	routeMitEntfernung2(Z, Ziel, [Z|Variable],Ergebnis, Distance1),
	Z \= Ziel,
	Start \= Ziel,
	Entfernung is Distance1 + Distance.


reiseMitEntfernung(Start,Ziel,Entfernung) :-
	distanz(Start,Ziel,Entfernung).

reiseMitEntfernung(Start,Ziel,Entfernung) :-
	distanz(Ziel,Start,Entfernung).






	
	
	
	
		
