mann(paul).
mann(ludwig).
mann(alberto).
mann(hans).
mann(max).
mann(andre).
mann(manuel).
frau(regina).
frau(lisa).
frau(franziska).
frau(sandra).
frau(martha).
frau(sarah).
frau(julia).
elternteil_von(franziska, paul).
elternteil_von(franziska, regina).
elternteil_von(hans, paul).
elternteil_von(hans, regina).
elternteil_von(sandra, paul).
elternteil_von(sandra, regina).
elternteil_von(max, ludwig).
elternteil_von(max, lisa).
elternteil_von(martha, ludwig).
elternteil_von(martha, lisa).
elternteil_von(sarah, sandra).
elternteil_von(sarah, max).
elternteil_von(julia, sandra).
elternteil_von(julia, max).
elternteil_von(manuel, sandra).
elternteil_von(manuel, max).
elternteil_von(andre, franziska).
elternteil_von(andre, alberto).

vater_von(Vater,Kind) :-
	elternteil_von(Kind,Vater),
	mann(Vater).
mutter_von(Mutter,Kind) :-
	elternteil_von(Kind,Mutter),
	frau(Mutter).

grossvater_von(Grossvater, Kind) :- 
	vater_von(X, Kind),
	vater_von(Grossvater, X);
	mutter_von(X, Kind),
	vater_von(Grossvater, X).

grossmutter_von(Grossmutter, Kind) :-
	mutter_von(Mutter, Kind),
	mutter_von(Grossmutter, Mutter);
	vater_von(Mutter, Kind),
	mutter_von(Grossmutter, Mutter).

bruder_von(Bruder,Person) :- 
	mann(Bruder),
	mann(X),
	elternteil_von(Person,X),
	elternteil_von(Bruder,X),
	not(Bruder==Person).
	
schwester_von(Schwester, Person) :-
	frau(Schwester),
	frau(X),
	elternteil_von(Person, X),
	elternteil_von(Schwester, X),
	not(Schwester==Person).

tante_von(Tante, Person):-
	frau(Tante),
	elternteil_von(Person, Z),
	schwester_von(Tante, Z),
	not(mutter_von(Tante, Person)).

onkel_von(Onkel, Person):-
	mann(Onkel),
	elternteil_von(Person, Z),
	bruder_von(Onkel, Z),
	not(vater_von(Onkel, Person)).


vorfahre_von(Vorfahr,Person):-
	elternteil_von(Person,Vorfahr);
	grossvater_von(Vorfahr, Person);
	grossmutter_von(Vorfahr, Person).




	

