package Lab3;

import java.util.Arrays;
import java.util.Random;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.util.random.WeightsRandomizer;

/**
 * Template class for prediction of sine values
 */
public class SineClass {
     private static double maxError = 0.05;
    public static void main(String[] args) {
        boolean manually = false;
        //double maxError = 0.05;
        double learnRate = 0.0001; //0.00000000000000001;  -> form ist schön, aber amplitude passt nicht
        int iterations = 10000000; //mit learnRate von 0.0001 ist die form ok und die amplitude passt besser bei 1000 durchläufen

        //---- Replace this part of code with your data generation 
        //---- Start 
        DataSet trainingSet = new DataSet(5, 1);
        // Two ways to fill trainingSet
        if (manually) {
            // create training set manually 
            trainingSet.addRow(new DataSetRow(new double[]{0, 0}, new double[]{0}));
            trainingSet.addRow(new DataSetRow(new double[]{0, 1}, new double[]{1}));
            trainingSet.addRow(new DataSetRow(new double[]{1, 0}, new double[]{1}));
            trainingSet.addRow(new DataSetRow(new double[]{1, 1}, new double[]{0}));
        } else {
            // read trainig set from file
            String inputFileName = "/home/rgn/NetBeansProjects/KI_WT2019_Lab3/src/Lab3/BSW220.txt"; // ensure correct path to file
            trainingSet = DataSet.createFromFile(inputFileName, 5, 1, "\t");
        }
        //--- End

        // create neural net: 3 layers with 2 input neurons, 3 hidden neurons, one oputput neuron
        MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(TransferFunctionType.TANH, 5, 24, 24, 1);
        neuralNet.randomizeWeights(new WeightsRandomizer(new Random(123)));

        // activate monitoring of learn process
        neuralNet.getLearningRule().addListener(new Utilities.LearningListener());
        // set learning rate
        neuralNet.getLearningRule().setLearningRate(learnRate);
        // set max error
        neuralNet.getLearningRule().setMaxError(maxError);
        // set maximum number of iterations
        neuralNet.getLearningRule().setMaxIterations(iterations);
        // start learning process
        neuralNet.learn(trainingSet);
        
        testNeuralNetwork(neuralNet, trainingSet);
    }

    /**
     *
     * @param nnet: current neural net
     * @param testSet: set of data
     */
    public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {
         //double maxError = 0.05;
        double d = 0.0;
        int cnt = 0;
        System.out.println("Testing trained neural network");
        for (DataSetRow dataRow : testSet.getRows()) {
            nnet.setInput(dataRow.getInput());
            nnet.calculate();
            double desOutput[] = dataRow.getDesiredOutput();
            double[] networkOutput = nnet.getOutput();
            for(int i = 0; i < networkOutput.length; i++) {
               d = Math.abs(desOutput[i] - networkOutput[i]);
                if(d >= maxError) {
                    d += d;
                    cnt++;
                    System.out.println("Error > maxError (" + d + " > " + maxError + ")");
                }
            }
          
            // System.out.print("Input: " + Arrays.toString(dataRow.getInput()));
            // System.out.println(" Output: " + Arrays.toString(networkOutput));

            // TBD. 
            // Add your code for the estimated value of
            // the network with the real value of the
            // training set (e.g. look at methods of DataSetRow)
        }
        System.out.println("Maxerror: " + (d / (double)cnt));
        Utilities.plot(nnet, testSet, 0.01);
    }
}
