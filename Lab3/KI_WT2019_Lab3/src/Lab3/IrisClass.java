package Lab3;

import java.util.Arrays;
import java.util.List;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;
import org.neuroph.util.data.norm.MaxMinNormalizer;
import org.neuroph.util.data.norm.Normalizer;

/**
 * Template class for classification of IRIS data
 */
public class IrisClass{

    public static void main(String[] args) {
        boolean manually = false;//true;
        String[] class_names = null;
        DataSet trainingSet = new DataSet(4, 3);
        DataSet testSet = new DataSet(4,3);
        // Two ways to get training data
        if (manually) {
            // create training set manually 
            trainingSet.addRow(new DataSetRow(new double[]{0, 0}, new double[]{0}));
            trainingSet.addRow(new DataSetRow(new double[]{0, 1}, new double[]{1}));
            trainingSet.addRow(new DataSetRow(new double[]{1, 0}, new double[]{1}));
            trainingSet.addRow(new DataSetRow(new double[]{1, 1}, new double[]{0}));
        } else {
            // read trainig set from file
            //0,0,1 Iris-setosa
            //0,1,0 Iris-versicolor
            //1,0,0 Iris-virginica
            String inputFileName = "/home/rgn/NetBeansProjects/KI_WT2019_Lab3/src/Lab3/iris2.data.txt"; // ensure correct path to file
            trainingSet = DataSet.createFromFile(inputFileName, 4, 3, ",");
            DataSet training = new DataSet(4,3);
            DataSet test = new DataSet(4,3);
            trainingSet.shuffle();
            training = trainingSet.split(80).get(0);
            test = trainingSet.split(20).get(0);
            
            Normalizer maxMin = new MaxMinNormalizer();
            maxMin.normalize(training);
            maxMin.normalize(test);
            trainingSet = training;
            testSet = test;
        }

        // create neural net: 3 layers with 2 input neurons, 3 hidden neurons, one oputput neuron
        MultiLayerPerceptron neuralNet = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, 4, 5, 3);

        // activate monitoring of learn process
        neuralNet.getLearningRule().addListener(new Utilities.LearningListener());
        // set learning rate
        neuralNet.getLearningRule().setLearningRate(0.1);
        // set max error
        neuralNet.getLearningRule().setMaxError(0.001);
        // set maximum number of iterations
        neuralNet.getLearningRule().setMaxIterations(1000);
        // start learning process
        neuralNet.learn(trainingSet);

        // test quality of neural network
        testNeuralNetwork(neuralNet, testSet);
    }

    public static boolean arrayCmp(double[] a, double[] b) {
        if (a.length == b.length) {
            for (int i = 0; i < a.length; i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static double[] roundArray(double[] a) {
        double[] b = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            if (a[i] >= .5) {
                b[i] = 1;
            } else {
                b[i] = 0;
            }
        }
        return b;
    }

    public static String getIrisType(double[] d) {
        double[] setosa = {0, 0, 1};
        double[] versicolor = {0, 1, 0};
        double[] virginica = {1, 0, 0};
      
        if (arrayCmp(d, setosa)) {
            return "Iris-setosa" ;
        }
        if (arrayCmp(d, versicolor)) {
            return "Iris-versicolor";
        }
        if (arrayCmp(d, virginica)) {
            return "Iris-virginica";
        }
        System.out.println("D: " + Arrays.toString(d));
        return "";
    }

    /**
     *
     * @param nnet: current neural net
     * @param testSet: set of data
     */
    public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {
        //0,0,1 Iris-setosa
        //0,1,0 Iris-versicolor
        //1,0,0 Iris-virginica
        double[] setosa = {0, 0, 1};
        double[] versicolor = {0, 1, 0};
        double[] virginica = {1, 0, 0};
        
  
        
        System.out.println("Testing trained neural network");
        String s = "";
        double errorcnt = 0.0, max = (double)testSet.size();
        for (DataSetRow dataRow : testSet.getRows()) {
            nnet.setInput(dataRow.getInput());
            nnet.calculate();
            double[] networkOutput = nnet.getOutput();
            double d[] = dataRow.getDesiredOutput();
            if (arrayCmp(dataRow.getDesiredOutput(), setosa)) {
               // System.out.println(Arrays.toString(roundArray(networkOutput)));
                s = getIrisType(roundArray(networkOutput));
                if(!s.equals("Iris-setosa")) {
                    errorcnt++;
                    s += " ERROR";
                }
                System.out.println("Iris-setosa " + s);
            }
            if (arrayCmp(dataRow.getDesiredOutput(), versicolor)) {
               // System.out.println(Arrays.toString(roundArray(networkOutput)));
                s = getIrisType(roundArray(networkOutput));
                if(!s.equals("Iris-versicolor")) {
                    errorcnt++;
                    s += " ERROR";
                }
                System.out.println("Iris-versicolor " + s );
            }
            if (arrayCmp(dataRow.getDesiredOutput(), virginica)) {
               // System.out.println(Arrays.toString(roundArray(networkOutput)));
                s = getIrisType(roundArray(networkOutput));
                if(!s.equals("Iris-virginica")) {
                    errorcnt++;
                    s += " ERROR";
                }
                System.out.println("Iris-virginica " + s );
            }
            
            /* System.out.print("Input: " + Arrays.toString(dataRow.getInput()));
            System.out.println(" Output: " + Arrays.toString(networkOutput));*/

        }
        System.out.println("Rate: " + (100.0 - ((errorcnt / max) * 100)));
        String[] classnames = {"Iris-setosa","Iris-versicolor","Iris-virginica"};
        Utilities.eval(nnet, testSet, classnames);
        /*False Positive Rate: ( 1 / 49 ) * 100 
          True Positive Rate: ()
        */
    }

    
}
