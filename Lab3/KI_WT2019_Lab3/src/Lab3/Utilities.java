package Lab3;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.events.LearningEvent;
import org.neuroph.core.events.LearningEventListener;
import org.neuroph.eval.ClassifierEvaluator;
import org.neuroph.eval.Evaluation;
import org.neuroph.eval.classification.ConfusionMatrix;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.data.norm.Normalizer;

/**
 * Class provides utilities for data evaluation
 */
public class Utilities{
    private static ConfusionMatrix cm = null;
    /**
     *
     * @param neuralNet: current neural network
     * @param dataSet: set of data
     * @param class_names: String array with names of classes; null will
     * indicate a binary class,
     */
    public static void eval(NeuralNetwork neuralNet, DataSet dataSet, String[] class_names) {
        if (neuralNet == null || dataSet == null) {
            return;
        }
        if (class_names != null && class_names.length != dataSet.getOutputSize()) {
            System.out.println("Error: Number of class names is not equal to the number of output neurons\n"
            + "       Please adapt the neural net accordingly");
            return;
        }
        Evaluation evaluation = new Evaluation();
        ClassifierEvaluator evaluator;
        if (class_names != null) {
            evaluation.addEvaluator(new ClassifierEvaluator.MultiClass(class_names));
            evaluation.evaluateDataSet(neuralNet, dataSet);
            evaluator = evaluation.getEvaluator(ClassifierEvaluator.MultiClass.class);
        } else {
            evaluation.addEvaluator(new ClassifierEvaluator.Binary(0.5));
            evaluation.evaluateDataSet(neuralNet, dataSet);
            evaluator = evaluation.getEvaluator(ClassifierEvaluator.Binary.class);
        }

        ConfusionMatrix confusionMatrix = evaluator.getResult();
        cm = confusionMatrix;
        System.out.println("Confusion matrix:\r\n");
        System.out.println(confusionMatrix.toString() + "\r\n\r\n");
        int fp = 0;
        int tp = 0;
        int tn = 0;
        int fn = 0;
        for (int i = 0; i < confusionMatrix.getClassLabels().length; i++){
          fp  += confusionMatrix.getFalsePositive(i);
          tp += confusionMatrix.getTruePositive(i);
          tn += confusionMatrix.getTrueNegative(i);
          fn += confusionMatrix.getFalseNegative(i);
        }
        double truePositiveRate = (double)(tp) / (double)(tp + fp);
        double falsePositiveRate = (double)(fp) / (double)(fp + tn);
        System.out.println("Flase Positive Rate: " + falsePositiveRate);
        System.out.println("True Positive Rate: " + truePositiveRate);
    }

    public static ConfusionMatrix GetConfusionsMatrix() {
        if (cm != null) {
            return cm;
        }
        return cm;
    }
    
    /**
     *
     * @param dataSet: set of data
     */
    public static void showDataSet(DataSet dataSet) {
        System.out.println("Show dataset");
        for (DataSetRow testSetRow : dataSet.getRows()) {
            System.out.println(testSetRow.toString());
        }
    }

   

    /**
     * Class support monitoring of training process
     */
    static class LearningListener implements LearningEventListener {

        int frequency = 500;

        @Override
        public void handleLearningEvent(LearningEvent event) {
            BackPropagation bp = (BackPropagation) event.getSource();
            if ((bp.getCurrentIteration() % frequency) == 0) {
                System.out.println("Current iteration : " + bp.getCurrentIteration());
                System.out.println("Error: " + bp.getTotalNetworkError());
            }
        }
    }

    /**
     *
     * @param neuralNet: current neural network
     * @param newDataSet: set of data
     * @param step: value to set distance between two data on the x-axis
     */
    public static void plot(NeuralNetwork neuralNet, DataSet newDataSet, double step) {
        if (neuralNet == null || newDataSet == null || step <= 0) {
            return;
        }
        XYSeriesCollection line_chart_dataset = new XYSeriesCollection();
        XYSeries series1 = new XYSeries("Predicted value");
        XYSeries series2 = new XYSeries("Real value");
        double count = 0;
        for (DataSetRow testSetRow : newDataSet.getRows()) {
            neuralNet.setInput(testSetRow.getInput());
            neuralNet.calculate();
            double[] real_Output = testSetRow.getDesiredOutput();
            double[] pred_Output = neuralNet.getOutput();
            series1.add(count, pred_Output[0]);
            series2.add(count, real_Output[0]);
            count += step;
        }
        line_chart_dataset.addSeries(series1);
        line_chart_dataset.addSeries(series2);
        XYDataset dataset = line_chart_dataset;
        JFreeChart lineChartObject = ChartFactory.createXYLineChart(
                "Value Prediction",
                "Input data",
                "Value",
                dataset, PlotOrientation.VERTICAL,
                true, true, false);
        ChartFrame frame = new ChartFrame("TestFrame", lineChartObject);
        frame.pack();
        frame.setVisible(true);
    }
}
